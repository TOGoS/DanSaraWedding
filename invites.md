## What's going on the invites?

Tell Brian:

- No "Three O Clock in the Afternoon"; let's just say "Ceremony starts at 3:00 PM"
- Website URL is https://www.theknot.com/us/sara-marry-dan
- Dan and Sara will be staying at the primitive campground at OnTwinLakes
- Alternative nearby campgrounds: TODO add info on features
  - K&C County Air
  - Hemlock Lake Campground
  - TP Resport & Campground
- Camping: Three camping options in the area, including one on-site where we'll be staying.
- You can find hotels and resorts in Rice Lake and Hayward
- Recommended Kennel in Rice Lake: Paws at Play Boarding & Daycare
  http://www.pawsatplayboarding.com/ - 715-931-0126
