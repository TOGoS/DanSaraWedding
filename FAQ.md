# Dan and Sara Wedding FAQ

## Is there a TheKnot page?

https://www.theknot.com/us/sara-metcalf-and-daniel-stevens-may-2018,
or, more shortly, https://www.theknot.com/us/sara-merry-dan

## Location

Wedding's going to be at On Twin Lakes near Birchwood, WI: http://www.ontwinlakes.com/


## Events and dates

Wedding's on *May 19th*.  Rehersal dinner's on the 18th.
The venue (On Twin Lakes) allows people to come in on the evening of the 17th to start setting up.
The 20th will be clean-up day.

I expect most guests (i.e. Sara's massive pool of relatives) will come for the wedding and then leave.
Wedding party (AND ANY OTHER CLOSE FRIENDS WHO COME FROM A LONG WAYS AWAY AND WANT TO HANG OUT)
should be around for the rehersal dinner, too.

There is 'primitive' camping at On Twin Lakes.  Dan and Sara will be camping there.



Q: Which birthdays celebrated when and where?  Who needs what gifts?

It's Waltmon's birthday, and also Bob's.  Bob never responded to my invitation e-mail though.

Q: Is rehearsal just a party?  Are important instructions to perform at wedding, dinner, etc provided at rehearsal and are thus mandatory for grooms/brides-people to attend?

We'll be doing a rehersal ceremony at the same location as the actual one
at 3 or 4 (I'm leaning towards 4 but need to make it official with Sara)
where we'll go over how everything's going to go, and then pizza at 5.

Q: How will wedding go?  Any specific walkings or readings?

It will go great!  Required readings include the bread book, The Communist Manifesto, and this talk by Rich Hickey: https://www.youtube.com/watch?v=rI8tNMsozo0

Q: Do people in the wedding sit apart from everyone else in the wedding?  What will seating arrangements be?

We'll probably reserve the front row for family and some close friends
who aren't in the wedding party.
Otherwise I don't care where anyone sits.

Q: Is there an opportunity to make speaches or presentations?  Are these expected?  Is audio/visual equipment available for presentations etc?

There's a projector screen inside the barn that we can use.
The ceremony itself will be outside and kept relatively short.
If A-Day has a presentation that he wants to do during dinner he should let us know how long it'll take
and we'll include that in our schedule which we haven't come up with yet.


## Transportation

Q: Bus from Madison?  Where departing?  Where arriving?  How long is ride?  Stops along the way for food etc?

Dan's family is renting a couple of large vans from Madison
which will leave early on Friday the 18th and return sometime on Sunday the 20th.
E-mail Dan and Dan's mom (togos 56 at gmail dot com and dianne at dianne and paul dot net)
if you'd like to ride with them.

Q: Flights and airlines into various airports.  Which airports are best?  Twin cities? Duluth? Eau Claire?  Car rental from these airports is available via...

Any of those would probably work well.
Devin will be flying into MSP.
MSP has the standard car rental companies.


## Hotels, camping, and accomodation

We're not blocking rooms anywhere, but we have some suggestions:

If you want to camp, you can camp at OnTwinLakes
(the wedding venue, who also happens to own a campgrounds near the wedding area)
one or both nights.
E-mail Rose at ontwinlakes.com.

Most of the nearby cheap hotels are in Rice Lake.

Some hotels that I know people will be staying at are:
- [Birchwood Motel](http://www.birchwoodmotelwi.com/rates.html) - (Tim, Jamie, at least)
- [Pullman Hotel](http://ricelakewis.com/listing/pullman-motel/) - (Some of Sara's friends probably staying here)
- [Best Western in Rice Lake](https://www.orbitz.com/Rice-Lake-Hotels-Best-Western-Inn.h21616.Hotel-Information?chkin=5%2F18%2F2018&chkout=5%2F20%2F2018&rm1=a2&regionId=6059765&hwrqCacheKey=a582648a-dd73-46d4-9365-080b0674028bHWRQ1523040725168&vip=false&c=f3cecf2d-f368-4075-a0d8-0601d038bbe2&mctc=10&exp_dp=88.49&exp_ts=1523040725149&exp_curr=USD&swpToggleOn=false&exp_pg=HSR) - (Our officiant Ken is staying here)


## Food

Who’s bringin which dishes

Who’s cooking what

What's the plan for alcohol?

If you are flying in, where should you buy food to bring to cook etc etc?

## CLimate, terrain, etc

Mosquitos expected, will have spray and tiki torches

Q: How are ticks in May at Stone Lake? Precautions to deal with ticks, if present?

Q: Precautions for rain (hopefully it won't, but must plan)?

The venue has a barn.  If it rains it just means we'll be confined to the barn instead of frolicking in the grass outside.


## Clothing

Everyone in the wedding party has the choice of a green dress, any style,
but preferrably matching the shade of green that Sara has some ribbons of,
or a black suit with a white shirt and a solid green tie
(same shade as the dresses, I suppose),
or some kind of black dress with green thingamabobs.

Q: Dress code for those not in wedding party?

A: Nope.


## Equipment, etc

Barn has electricity.

Due to disappointment with sound systems at other people's weddings,
Dan plans to set up his own sound system and play his own music.
Got any song requests?  Send them in now so I can download them.

Q: Do they have a projector or do we need to bring ours?


## Gifts

Dan and Sara have all the stuff they need.
Monetary donations are welcome.
We'll put them towards a new kitchen.




## Emergencies

If there is a medical emergency, where is the nearest hospital etc?

Is cell reception decent enough for emergency calls?

Are any allergies anticipated?  Bee stings etc?  Peanuts?  Epipens or other stuff available?

Will anyone with basic medical skills be available among us, and what's their contact info?

