List of stuff to make sure it gets shipped up


## Key

- Need
B Boxed up, ready for car
* Packed in car




## Sound Equipment

(see also [sound-systems.md](sound-systems.md))

- Speaker stands (4)
- Speakers
  * 4 large
  - 4 small
- Amplifiers
  - 2 x Lepy/Lepai 2020s
  B This proish 2x700W one: https://smile.amazon.com/gp/product/B075YHL3TF
  - A car one: https://smile.amazon.com/gp/product/B00HLSU6PI
  - Power supply / batteries / cables
- 'Portable sound system' - power supply, mixer, microphone adapter
- Microphone
- Speaker cable
  - Whatever scrappy ones + box full should be more than sufficient
- Audio cables
- Audio adapters (
- Laptop
- Backup laptop (install VDJ!)
- Hard drive
- Backup hard drive


## Party Lighting

- 4 x Striped ball LED light string; 3m, 3xAA each
- 3 x Fairy light strands; 8 lights, 2xAA each; Battery holder has a resistor or something but 3V across the LEDs works
- EL wire
- Lots of AAs


## Misc

- Velcro
- Tape
- Scissors
- Pliers
- Wire cutters/strippers
- Soldering iron
- Crimper, connectors, #6 screws
- #10 screws (there are a few in pocket of portable rack)
- uUSB cables for charging phones
- 'solar' USB charger

## On ya