#!/usr/bin/env php
<?php

require_once 'vendor/autoload.php';

# Process the guest list,
# try to figure out what actual addresses we need
# Usage: extract-addresses <guest-list.csv

class DanSaraWedding_GuestListCSVParser extends TOGoS_PHPipeable_AbstractFilter
{
	protected $headers = null;
	protected $addressColumnIndex = 2;
	protected $namesColumnIndex = 0;
	protected $addresseeColumnIndex = 1;
	protected $inviteCountColumnIndex = 4;
	protected $notesColumnIndex = 3;
	protected $errors = array();
	protected $entries = array();
	protected $entriesByAddress = array();
	protected $addresslessEntries = array();
	
	protected function headerRow($row, array $metadata) {
		$this->headers = $row;
	}
	
	protected function parseList($list) {
		$list = trim($list);
		if( $list == '' ) return array();
		$list = explode(',', $list);
		foreach( $list as $k=>&$item ) {
			$item = trim($item);
			if( $item == '' ) unset($list[$k]);
		}
		unset($item);
		return $list;
	}

	protected function validateAddress($address, array &$errors=array()) {
		if( empty($address) ) {
			$errors[] = array("Address string is empty");
			return null;
		}
		$parts = preg_split('/,\s*/', $address);
		// 4325 Winnequah Rd, Madison, Wi 53716"
		// 4325 Winnequah Rd, Apt 123, Madison, Wi 53716"
		$streetAddress = $parts[0];
		$maybeCountryCode = $parts[count($parts)-1];
		if( $maybeCountryCode == 'USA' || $maybeCountryCode == 'France' || $maybeCountryCode == 'Sweden' ) {
			$countryCode = array_pop($parts);
		} else {
			$countryCode = 'USA';
		}
		if( $countryCode != 'USA' ) {
			return array(
				'idkLines' => implode("\n", $parts),
				'countryCode' => $countryCode
			);
		}
		if( count($parts) == 3 ) {
			$cityName = $parts[1];
			$stateAndZip = $parts[2];
			$unitCode = null;
		} else if( count($parts) == 4 ) {
			$unitCode = $parts[1];
			$cityName = $parts[2];
			$stateAndZip = $parts[3];
		} else {
			$errors[] = array("Address has a weird (".count($parts).") number of comma-delimited parts");
			return null;
		}
		
		if( !preg_match('/^[A-Z]{2} \d{5}(?:-\d{4})?$/', $stateAndZip) ) {
			$errors[] = array("Malformed state/zip code: $stateAndZip");
			return null;
		}
		
		list($stateCode,$postalCode) = explode(" ",$stateAndZip);
		
		return array(
			'streetAddress' => $streetAddress,
			'unitCode' => $unitCode,
			'cityName' => $cityName,
			'stateCode' => $stateCode,
			'postalCode' => $postalCode,
			'countryCode' => $countryCode,
		);
	}
	
	protected function addressee($names, $addressee) {
		if( !empty($addressee) ) return $addressee;
		foreach( $names as &$n ) {
			if( $n == 'etc' ) $n = 'company';
		}
		return EarthIT_Schema_WordUtil::oxfordlyFormatList($names, 'and');
	}

	protected static function actuallyEmpty($value) {
		return $value === '' || $value === null || is_array($value) && count($value) == 0;
	}
	
	protected function dataRow($row, array $metadata) {
		$notes = $row[$this->notesColumnIndex];
		if( $notes == 'Totals' ) return;
		$names = $this->parseList($row[$this->namesColumnIndex]);
		$addressee = $row[$this->addresseeColumnIndex];
		$addressee = $this->addressee($names, $addressee);
		$address = $row[$this->addressColumnIndex];
		if( self::actuallyEmpty($address) ) $address = null;
		$inviteCount = $row[$this->inviteCountColumnIndex];
		$inviteCount = self::actuallyEmpty($inviteCount) ? null : (int)$inviteCount;
		if( self::actuallyEmpty($names) && self::actuallyEmpty($addressee) && self::actuallyEmpty($address) ) {
			// lol do nuthin
			return;
		}
		$lineNumber = $metadata['lineNumber'];
		if( self::actuallyEmpty($names) ) {
			$this->errors[] = array(
				'lineNumber' => $lineNumber,
				'message' => "No 'name'",
			);
		}
		$entry = array(
			'lineNumber' => $lineNumber,
			'names' => $names,
			'addressee' => $addressee,
			'address' => $address,
			'inviteCount' => $inviteCount,
		);
		if( $address ) {
			$this->entriesByAddress[$address][] = $entry;
			$addressProbz = array();
			$parsedAddress = $this->validateAddress($address, $addressProbz);
			if( $parsedAddress === null ) {
				$this->errors[] = array(
					'lineNumber' => $lineNumber,
					'message' => "Address string is invalid: ".implode('; ', $addressProbz).": {$address}"
				);
			} else {
				$entry['parsedAddress'] = $parsedAddress;
			}
		} else {
			$this->addresslessEntries[] = $entry;
		}
		$this->entries[] = $entry;
		$this->emitItem($entry, $metadata);
	}
	
	public function item($row, array $metadata=array()) {
		if( $this->headers === null ) {
			$this->headerRow($row, $metadata);
		} else {
			$this->dataRow($row, $metadata);
		}
	}
	public function __invoke($item, array $metadata=array()) {
		$this->item($item, $metadata);
	}
	
	public function close(array $metadata=array()) {
		return array(
			'errors' => $this->errors,
			'entries' => $this->entries,
			'entriesByAddress' => $this->entriesByAddress,
			'addresslessEntries' => $this->addresslessEntries,
		);
	}
}

class DanSaraWedding_CSVOutputSink implements TOGoS_PHPipeable_Sink
{
	protected $stream;
	protected $headers;
	public function __construct($stream, array $headers=array()) {
		$this->stream = $stream;
		$this->headers = $headers;
	}
	public function open(array $md=array()) {
		fputcsv($this->stream, $this->headers);
	}
	public function item($item, array $md=array()) {
		fputcsv($this->stream, $item);
	}
	public function close(array $md=array()) { }
}

function formatAddress( $addressInfo ) {
	$partList = array(
		'idkLines',
		'streetAddressAndUnit',
		'cityStateZip',
		'countryCode',
	);
	$lines = array();
	foreach( $partList as $k ) {
		if( isset($addressInfo[$k]) ) {
			$lines[] = $addressInfo[$k];
		} else if( $k == 'streetAddressAndUnit' ) {
			$streetAddress_unit = array();
			if( isset($addressInfo['streetAddress']) ) $streetAddress_unit[] = $addressInfo['streetAddress'];
			$sep = ', ';
			if( isset($addressInfo['unitCode']) ) {
				$streetAddress_unit[] = $addressInfo['unitCode'];
				if( preg_match('/^#/', $addressInfo['unitCode']) ) $sep = ' '; // No comma before '#'
			}
			if( !empty($streetAddress_unit) ) $lines[] = implode($sep, $streetAddress_unit);
		} else if( $k == 'cityStateZip' ) {
			$city_stateZip = array();
			if( isset($addressInfo['cityName']) ) $city_stateZip[] = $addressInfo['cityName'];
			$state_zip = array();
			if( isset($addressInfo['stateCode']) ) $state_zip[] = $addressInfo['stateCode'];
			if( isset($addressInfo['postalCode']) ) $state_zip[] = $addressInfo['postalCode'];
			if( !empty($state_zip) ) $city_stateZip[] = implode(' ',$state_zip);
			if( !empty($city_stateZip) ) $lines[] = implode(', ', $city_stateZip);
		}
	}
	return implode("\n", $lines);
}

function formatAddressEntry($entry) {
	$address = $entry['address'];
	if( empty($address) ) return null;
	$addressee = $entry['addressee'];
	$reformattedAddress = isset($entry['parsedAddress']) ? formatAddress($entry['parsedAddress']) : '';
	return [$address, $addressee, $reformattedAddress];
}

$instream = fopen("php://stdin", "rb");
$outstream = fopen('php://stdout', 'wb');

$pipeline = TOGoS_PHPipeable_Pipeline::create([
	new DanSaraWedding_GuestListCSVParser(),
	new TOGoS_PHPipeable_Filter('formatAddressEntry', true),
	new DanSaraWedding_CSVOutputSink($outstream, array('Address','Addressee','FormattedAddress')),
]);

$perforator = new DanSaraWedding_GuestListCSVParser();
$pipeline->open();
$lineNumber = 1;
while( ($row = fgetcsv($instream)) ) {
	$pipeline($row, array('lineNumber'=>$lineNumber));
	++$lineNumber;
}
$result = $pipeline->close();

$everythingGreat = true;
$otherProblems = array();
foreach( $result['entries'] as $entry ) {
	if( $entry['address'] !== null && $entry['inviteCount'] === null ) {
		$otherProblems[] = array(
			'lineNumber' => $entry['lineNumber'],
			'message' => "{$entry['addressee']} has an address but no invite count"
		);
	}
	if( $entry['address'] === null && $entry['inviteCount'] > 0 ) {
		$otherProblems[] = array(
			'lineNumber' => $entry['lineNumber'],
			'message' => "{$entry['addressee']} has an invite count but no address"
		);
	}
}
if( count($result['addresslessEntries']) ) {
	fwrite(STDERR, "\e[91m".count($result['addresslessEntries'])." entries lack addresses:\e[39m\n");
	foreach( $result['addresslessEntries'] as $entry ) {
		fwrite(STDERR, "  line {$entry['lineNumber']}: {$entry['addressee']}\n");
	}
	$everythingGreat = false;
}
foreach( $result['entriesByAddress'] as $address=>$entries ) {
	if( count($entries) > 1 ) {
		$otherProblems[] = array(
			'message' => "Multiple entries for address: $address:\n  ".implode("\n  ",array_map(function($e) { return $e['addressee']; }, $entries))
		);
	}
}
if( isset($result['errors']) && count($result['errors']) ) {
	fwrite(STDERR, "\e[91mFormatting Errors!\e[39m\n");
	foreach($result['errors'] as $error) {
		fwrite(STDERR, "  line {$error['lineNumber']}: {$error['message']}\n");
	}
	$everythingGreat = false;
}
if( count($otherProblems) ) {
	fwrite(STDERR, "\e[91mOther problems!\e[39m\n");
	foreach( $otherProblems as $prob ) {
		if( isset($prob['lineNumber']) ) {
			fwrite(STDERR, "  line {$prob['lineNumber']}: ".str_replace("\n","\n  ",$prob['message'])."\n");
		} else {
			fwrite(STDERR, "  ".str_replace("\n","\n  ",$prob['message'])."\n");
		}
	}
	$everythingGreat = false;
}
exit($everythingGreat ? 0 : 1);
