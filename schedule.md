### 17th - Thursday

Venue allows us to be there from 18:00-21:00.

- Dan, Sara, Joh, Sara's mom, various siblings

Bring:

- Sound system
- Benches
- Decorations for barn and 'yard' (ceremony area) (lights, flowers)
- Party tent
- Camping tents


## 18th - Friday

Have access to venue from 12:00-22:00.

Dan, Sara, Sara's mom, various siblings

Assemble:

- Main sound system (in barn)
- tables
- benches
- barn and yard decorations (including live plants and lights)
- Silo sound systems

### 15:00

Should have wedding party, photographer, officiant, singers

- Rehersal
  - Ceremony
  - Sounds/music/microphone
  - Lights
  - Photos

- Put up tents

### 17:00

- Dinner
- Get tuxes/suits/shoes/etc into groom silo
- Get desses/shoes/makeup/etc into bride silo

### Later

- Sara will run off with whomever to see Deadpool 2

### 21:30

Get shut down


## 19th - Saturday

### 12:00

Sara:

- get hair done
- Pick up flowers
- Head back to venue by 13:00

### 14:00

- Wedding party photos at barn

### 15:00

- Ceremony
- Photos with family
- Snacks and refreshments provided by caterer
- Go to cabin for more photos and cocktails

### 17:00

- Dinner

### 18:00

- Dessert
- Games
- Q & A

### 19:00

- Dancing inside barn
- Light snacks and unspiked punch

### 21:00

- Migrate sound system outside

### 22:00

- After-dance dance and s'mores

### 24:00

- No more booze

### 24:30

- Quiet time


## 20th - Sunday

### 07:00

- Breakfast spread and coffee available
- Start packing

### 10:00

- Brunch
- continue packing

### 12:00

- Have everything packed and leave
