all: addresses.csv

addresses.csv: guest-list.csv guest-list-processor
	guest-list-processor/extract-addresses <"$<" | tee "$@"

guest-list-processor: $(shell find guest-list-processor/extract-addresses)
	${MAKE} -C "$@"
	touch "$@"
