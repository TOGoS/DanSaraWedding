# To-do for wedding!

Mostly for Dan; Sara's got her own list.

- Make a bouquet for throwing
  - "Started!" -- Sara
- Make a garter for throwing
- Design table decorations


### Food, booze

- Get stuff for s'mores for wedding night


### Equipment

- Fill out [list of stuff you plan to bring](equipment.md)
* Test that it works
- Decide how to pack and ship to venue
  - Keep a list of what gets sent with whom!

- If I'm doing any weird WiFi stuff I need to make sure I have a network set up
  (on an RPi or an ESP8266) and reprogram devices to use it.

### Music

- Make a folder for things downloaded for the wedding
- download all the songs we want to play
- make master playlist(s)
- external hard drive and backup full of songs
- Test out sound system in basement (wear ear pro!)
- Pick out slideshow pictures
  * all of TOGoS/photos/2017


## Done!

* Figure out venue
* Make theknot.com site
* Make invitee list
* Design, print, send invites
* Get marriage license
* Buy suit
* Take dance lessons
* Figure out placement of tables in barn
* Pick out and buy matching ties from https://www.solidcolorneckties.com/irish-green-staff-tie.html
* Hire caterer
* Figure out Booze
  * Half keg Spotted Cow
  * Half keg Miller Lite
  * Half keg Cider
* Buy bouquets
  * One for Sara
  * One for throwing
  * One for each of 'girls'
  * Corsages (things for wrists) for Coreen, Ruthana, Katie
  * Flower crowns for everybody
  * boots and ears
* Get wedding ring for Dan
* Make [schedule](schedule.md)
* Get a stand-in wedding ring for Sara
* Advise Dawne about beer for 18th
